﻿Imports CounterLib

Public Class Form1
    Dim Compteur As New Counter

    Private Sub Btnplus_Click(sender As Object, e As EventArgs) Handles Btnplus.Click
        Compteur.increment()
        lblval.Text = Compteur.GetValeur()
    End Sub
    Private Sub Btnmoin_Click(sender As Object, e As EventArgs) Handles Btnmoin.Click
        Compteur.decrement()
        lblval.Text = Compteur.GetValeur()
    End Sub

    Private Sub BtnRAZ_Click(sender As Object, e As EventArgs) Handles BtnRAZ.Click
        Compteur.RAZ()
        lblval.Text = Compteur.GetValeur()
    End Sub

End Class
