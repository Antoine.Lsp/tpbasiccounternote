﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CounterLib
{
    public class Counter
    {
        private static int valeur;

        public int GetValeur()
        {
            return Counter.valeur;
        }

        public void SetValeur(int valeur)
        {
            Counter.valeur = valeur;
        }

        public void increment()
        {
            Counter.valeur = Counter.valeur + 1;
        }

        public void decrement()
        {
            Counter.valeur = Counter.valeur - 1;

        }

        public void RAZ()
        {
            Counter.valeur = 0;
        }
    }
}