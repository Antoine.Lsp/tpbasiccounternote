﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btnmoin = New System.Windows.Forms.Button()
        Me.BtnRAZ = New System.Windows.Forms.Button()
        Me.Btnplus = New System.Windows.Forms.Button()
        Me.lblval = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btnmoin
        '
        Me.Btnmoin.Location = New System.Drawing.Point(95, 151)
        Me.Btnmoin.Name = "Btnmoin"
        Me.Btnmoin.Size = New System.Drawing.Size(75, 32)
        Me.Btnmoin.TabIndex = 0
        Me.Btnmoin.Text = "-"
        Me.Btnmoin.UseVisualStyleBackColor = True
        '
        'BtnRAZ
        '
        Me.BtnRAZ.Location = New System.Drawing.Point(262, 229)
        Me.BtnRAZ.Name = "BtnRAZ"
        Me.BtnRAZ.Size = New System.Drawing.Size(98, 32)
        Me.BtnRAZ.TabIndex = 1
        Me.BtnRAZ.Text = "RAZ"
        Me.BtnRAZ.UseVisualStyleBackColor = True
        '
        'Btnplus
        '
        Me.Btnplus.Location = New System.Drawing.Point(427, 151)
        Me.Btnplus.Name = "Btnplus"
        Me.Btnplus.Size = New System.Drawing.Size(75, 32)
        Me.Btnplus.TabIndex = 2
        Me.Btnplus.Text = "+"
        Me.Btnplus.UseVisualStyleBackColor = True
        '
        'lblval
        '
        Me.lblval.AutoSize = True
        Me.lblval.Font = New System.Drawing.Font("Impact", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblval.Location = New System.Drawing.Point(284, 136)
        Me.lblval.MaximumSize = New System.Drawing.Size(100, 100)
        Me.lblval.Name = "lblval"
        Me.lblval.Size = New System.Drawing.Size(41, 48)
        Me.lblval.TabIndex = 3
        Me.lblval.Text = "0"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Impact", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(254, 66)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(106, 48)
        Me.lblTotal.TabIndex = 4
        Me.lblTotal.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(598, 498)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblval)
        Me.Controls.Add(Me.Btnplus)
        Me.Controls.Add(Me.BtnRAZ)
        Me.Controls.Add(Me.Btnmoin)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btnmoin As Button
    Friend WithEvents BtnRAZ As Button
    Friend WithEvents Btnplus As Button
    Friend WithEvents lblval As Label
    Friend WithEvents lblTotal As Label
End Class
