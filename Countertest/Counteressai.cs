﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterLib;

namespace Countertest
{
    [TestClass]
    public class Counteressai
    {
        [TestMethod]
        public void TestGetValeur()
        {
            Counter Compteur = new Counter();
            Compteur.SetValeur(0);
            Assert.AreEqual(0, Compteur.GetValeur());
           
        }
        [TestMethod]
        public void TestSetValeur()
        {
            Counter Compteur = new Counter();
            Compteur.SetValeur(10);
            Assert.AreEqual(10, Compteur.GetValeur());
            
        }
        [TestMethod]
        public void TestADD()
        {
            Counter Compteur = new Counter();
            Compteur.SetValeur(7);
            Compteur.increment();
            Assert.AreEqual(8, Compteur.GetValeur());
          
        }
        [TestMethod]
        public void TestRemove()
        {
            Counter Compteur = new Counter();
            Compteur.SetValeur(10);
            Compteur.decrement();
            Assert.AreEqual(9, Compteur.GetValeur());
            
        }
        [TestMethod]
        public void TestRAZ()
        {
            Counter Compteur = new Counter();
            Compteur.SetValeur(10);
            Compteur.RAZ();
            Assert.AreEqual(0, Compteur.GetValeur());
        }
    }
}
